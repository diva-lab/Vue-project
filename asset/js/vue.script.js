// initializing vue app 
var app = new Vue({

	el:'#app' , 
	data:{
		count : 0 
	},
	methods:{
		countUp : function(){
			this.count += 1
		}, 
		countDown: function(){
			if(this.count != 0 ){
			this.count -= 1
			}

		}
	}
	 
}); 